<?php

/**
 * @file theme.inc
 */

/**
 * Outputs HTML for the view/hide toggle. 
 *
 * @ingroup themeable
 */
function theme_date_repeat_themeable_toggle($vars) {
  $view_text = t('View all dates');
  $hide_text = t('Hide all dates');
  
  // Make this text available to our javascript file.
  drupal_add_js(array('date_repeat_themeable' => array('toggle_view' => $view_text)), 'setting');
  drupal_add_js(array('date_repeat_themeable' => array('toggle_hide' => $hide_text)), 'setting');
  
  // Load .js and .css files for toggle.
  drupal_add_js(drupal_get_path('module', 'date_repeat_themeable') . '/date_repeat_themeable.js', array('weight' => JS_THEME));
  drupal_add_css(drupal_get_path('module', 'date_repeat_themeable') . '/date_repeat_themeable.css');

  $output = '<span class="date-repeat-themeable-toggle">';
  $output .= '<a href="#">' . $view_text . '</a>';
  $output .= '</span>';
  
  return $output;
}

/**
 *  
 * @ingroup themeable
 */
function theme_date_repeat_themeable_rrule($vars) {
  $field = $vars['field'];
  $items = $vars['items'];
  
  $output  = '<div class="date-repeat-pre">';
  $output .= date_repeat_themeable_rrule_description($field, $items[0]['rrule']);  
  $output .= theme('date_repeat_themeable_toggle', $vars);
  $output .= '</div>';
  return $output;
}

/**
 *  
 * @ingroup themeable
 */
function theme_date_repeat_themeable_rrule_status($vars) {
  $output = '';
  $status = $vars['status'];
  $next_date = $vars['next_date'];

  if ($status == 'upcoming' && $next_date !== FALSE) {
    $output .= trim(t('Next date: !next_date.', array('!next_date' => $next_date)));
  }
  elseif ($status == 'ended') {
    $output .= trim(t('Ended.'));
  }
  return $output;
}

/**
 *  
 * @ingroup themeable
 */
function theme_date_repeat_themeable_rrule_description($vars) {
  $description = $vars['description'];
  
  return t('Repeats !interval !bymonth !byday !count !until' . ($description['!except'] ? ' (!except)' : '') . '. !additional !status', $description);
}

/**
 *  
 * @ingroup themeable
 */
function theme_date_repeat_themeable_rrule_interval($vars) {
  $output = '';
  
  $frequency = $vars['frequency'];
  $interval = $vars['interval'];
  
  switch ($frequency) {
    case 'WEEKLY':
      $output .= format_plural($interval, 'weekly', 'every @count weeks') . ' ';
      break;
    case 'MONTHLY':
      $output .= format_plural($interval, 'monthly', 'every @count months') . ' ';
      break;
    case 'YEARLY':
      $output .= format_plural($interval, 'yearly', 'every @count years') . ' ';
      break;
    default:
      $output .= format_plural($interval, 'daily', 'every @count days') . ' ';
      break;
  }
  return $output;
}

/**
 *  
 * @ingroup themeable
 */
function theme_date_repeat_themeable_rrule_byday($vars) {
  $output = '';
  
  $count = $vars['count'];
  $ordinals = $vars['ordinals'];
  $day = $vars['day'];
  
  if (!empty($count)) {
    // See if there is a 'pretty' option for this count, e.g. "first".
    $order = array_key_exists($count, $ordinals) ? strtolower($ordinals[$count]) : $count;
    $output .= trim(t('!repeats_every_interval on the !date_order !day_of_week', array('!repeats_every_interval ' => '', '!date_order' => $order, '!day_of_week' => $day)));
  }
  else {
    $output .= trim(t('!repeats_every_interval every !day_of_week', array('!repeats_every_interval ' => '', '!day_of_week' => $day)));
  }
  return $output;
}


/**
 *  
 * @ingroup themeable
 */
function theme_date_repeat_themeable_rrule_byday_join($vars) {
  $results = $vars['results'];
  return implode(' ' . t('and') . ' ', $results);
}

/**
 *  
 * @ingroup themeable
 */
function theme_date_repeat_themeable_rrule_bymonth($vars) {
  $results = $vars['results'];
  return trim(t('!repeats_every_interval on !month_names', array('!repeats_every_interval ' => '', '!month_names' => implode(', ', $results))));
}

/**
 *  
 * @ingroup themeable
 */
function theme_date_repeat_themeable_rrule_bymonthday($vars) {
  $month_days = $vars['month_days'];
  $results = $vars['results'];
  return trim(t('!repeats_every_interval on the !month_days of !month_names', array('!repeats_every_interval ' => '', '!month_days' => implode(', ', $month_days), '!month_names' => implode(', ', $results))));
}

/**
 *  
 * @ingroup themeable
 */
function theme_date_repeat_themeable_rrule_count($vars) {
  $count = $vars['count'];
  return trim(t('!repeats_every_interval !count times', array('!repeats_every_interval ' => '', '!count' => $count)));
}

/**
 *  
 * @ingroup themeable
 */
function theme_date_repeat_themeable_rrule_until($vars) {
  $until = $vars['until'];
  $format = $vars['format'];
  return trim(t('!repeats_every_interval until !until_date', array('!repeats_every_interval ' => '', '!until_date' => date_format_date($until, 'custom', $format))));
}

/**
 *  
 * @ingroup themeable
 */
function theme_date_repeat_themeable_rrule_except($vars) {
  $values = $vars['values'];
  return trim(t('!repeats_every_interval except !except_dates', array('!repeats_every_interval ' => '', '!except_dates' => implode(', ', $values))));
}

/**
 *  
 * @ingroup themeable
 */
function theme_date_repeat_themeable_rrule_week_starts_on($vars) {
  $day_of_week = $vars['day_of_week'];
  return trim(t('!repeats_every_interval where the week start on !day_of_week', array('!repeats_every_interval ' => '', '!day_of_week' => $day_of_week)));
}

/**
 *  
 * @ingroup themeable
 */
function theme_date_repeat_themeable_rrule_additional($vars) {
  $values = $vars['values'];
  return trim(t('Also includes !additional_dates.', array('!additional_dates' => implode(', ', $values))));
}

/**
 *  
 * @ingroup themeable
 */
function theme_date_repeat_themeable_dates($vars) {
  $field = $vars['field'];
  $items = $vars['items'];
  
  $output = '<div class="date-repeat-dates">';
  $output .= '<span class="date-repeat-dates-label"><em>' . t('All scheduled dates:') . '</em></span>';
  foreach (array_keys($items) as $key) {
    $output .= $field[$key]['#markup'];
  }
  $output .= '</div>';
  return $output;
}
