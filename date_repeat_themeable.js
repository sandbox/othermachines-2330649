(function ($) {
  Drupal.behaviors.date_repeat_themeable = {
    attach: function(context, settings) {
      $('.date-repeat-themeable-toggle').click(
        function(event) {
          $(this).parent().siblings('.date-repeat-dates').toggle("fast",
            function () {
              if (this.style.display != 'none') {
                $(this).siblings('.date-repeat-pre').find('.date-repeat-themeable-toggle a').html(Drupal.settings.date_repeat_themeable.toggle_hide);
              }
              else {
                $(this).siblings('.date-repeat-pre').find('.date-repeat-themeable-toggle a').html(Drupal.settings.date_repeat_themeable.toggle_view);
              }
            }
          );
          return false;
        }
      );
    }
  }
})(jQuery);

