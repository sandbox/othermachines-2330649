
Themeable Repeating Dates

This is a replacement for another sandbox project, Date Repeat FM[1]. 

A work in progress! Not ready for production. See TO DO. 

What it does:

- takes over Date[2] module's default handling of the repeat rule string,
- makes the repeat rule string (more) themeable,
- provides some default theming including a javascript toggle to show or hide 
  dates.

It does NOT:

- provide a field formatter,
- create entities/nodes for each repeating date (for that, see Date Repeat Entity[3]),
- provide an interface for editing individual dates in a series (for that, see Date Repeat Instance[4])
- attempt to alter the output of individual date strings.


TO DO
-------------------------

- Storing the true value of 'show_repeat_rule' in another variable 
  ('show_repeat_rule_store') won't play well with field settings. 
  See:
  hook_date_field_formatter_settings_form_alter() (date/date.api.php)
  hook_date_field_formatter_settings_summary_alter() (date/date.api.php)
  date_old_formatter_get_settings() (date/date.field.inc)

- Theme for different displays (teaser, full node). 

- 'Cache dates' field option may interfere with this module's functionality
  if theme output depends on current date.
  

Hook implementations
-------------------------

* hook_date_formatter_dates_alter() (date/date.api.php)
  - Here is where we can alter/access the processed dates output by 
    date_formatter_process(). We are checking if a repeat rule should be shown; 
    if so, send it to helper function date_repeat_themeable_dates() for storage 
    in a static variable. 
 
* hook_date_formatter_pre_view_alter() (date/date.api.php)
  - The Date module adds a repeat rule as the first item in the date series 
    array if show_repeat_rule == 'show'. To prevent this, and to prevent Date 
    module from formatting our repeat rule, we are changing the 
    value of this variable to 'hide' and storing the actual value in a new
    variable called 'show_repeat_rule_store'.
  
* hook_field_attach_view_alter() (field/field.api.php)
  - Alter the output of the date repeat field. Introduce two theme functions -
    one for the repeat rule and one for the date output. Unset the entire
    date series in $element[0], [1], [2], etc. and instead combine all markup  
    into a single item at $element[0]. 
  
* hook_theme() (system/system.api.php)


Helper functions
-------------------------

* date_repeat_themeable_dates()
  - Called from date_repeat_themeable_date_formatter_dates_alter() (date_repeat_themeable.module)
  - Function to store and retrieve dates in/from a static variable.
  
* date_repeat_themeable_rrule_description()
  - Called from theme_date_repeat_themeable_rrule() (date_repeat_themeable.theme.inc)
  - A rewrite of date_repeat_rrule_description(). Since we have tricked the Date
    module into thinking a repeat rule is not wanted (see README.txt description 
    for hook_date_formatter_pre_view_alter()), we can now take over the job. This 
    function constructs a human-readable description of the rule - now with new 
    theme functions added. 
  

Theme functions
-------------------------

* theme_date_repeat_themeable_toggle()
  - Called from theme_date_repeat_themeable_rrule() (date_repeat_themeable.theme.inc)
  - Theme the view/hide toggle that controls the display of the repeating date series.
  
* theme_date_repeat_themeable_rrule()
  - Called from date_repeat_themeable_field_attach_view_alter() (date_repeat_themeable.module)
  - Theme the repeat rule.

* theme_date_repeat_themeable_dates()
  - Called from date_repeat_themeable_field_attach_view_alter() (date_repeat_themeable.module)
  - Theme the date output.
  
* theme_date_repeat_themeable_rrule_status()
  - Called from date_repeat_themeable_rrule_description() (date_repeat_themeable.module)

* theme_date_repeat_themeable_rrule_description()
  - Called from date_repeat_themeable_rrule_description() (date_repeat_themeable.module)

* theme_date_repeat_themeable_rrule_interval()
  - Called from date_repeat_themeable_rrule_description() (date_repeat_themeable.module)

* theme_date_repeat_themeable_rrule_byday()
  - Called from date_repeat_themeable_rrule_description() (date_repeat_themeable.module)

* theme_date_repeat_themeable_rrule_byday_join()
  - Called from date_repeat_themeable_rrule_description() (date_repeat_themeable.module)

* theme_date_repeat_themeable_rrule_bymonth()
  - Called from date_repeat_themeable_rrule_description() (date_repeat_themeable.module)

* theme_date_repeat_themeable_rrule_bymonthday()
  - Called from date_repeat_themeable_rrule_description() (date_repeat_themeable.module)

* theme_date_repeat_themeable_rrule_count()
  - Called from date_repeat_themeable_rrule_description() (date_repeat_themeable.module)

* theme_date_repeat_themeable_rrule_until()
  - Called from date_repeat_themeable_rrule_description() (date_repeat_themeable.module)

* theme_date_repeat_themeable_rrule_except()
  - Called from date_repeat_themeable_rrule_description() (date_repeat_themeable.module)

* theme_date_repeat_themeable_rrule_week_starts_on()
  - Called from [not called]

* theme_date_repeat_themeable_rrule_additional()
  - Called from date_repeat_themeable_rrule_description() (date_repeat_themeable.module)

  
[1] https://www.drupal.org/sandbox/othermachines/1456552
[2] http://drupal.org/project/date
[3] https://www.drupal.org/project/date_repeat_entity
[4] https://www.drupal.org/project/date_repeat_instance